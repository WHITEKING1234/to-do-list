//
//  AppCordinator.swift
//  To Do List
//
//  Created by Mac on 23/5/23.
//

import Foundation
import UIKit

final class AppCoordinator {
    
    private let window: UIWindow?
    
    init(window: UIWindow?) {
        self.window = window
    }

    func start() {
        openRootViewController()
    }
    
//    fileprivate func openRootViewController() {
//        window?.rootViewController = UINavigationController(rootViewController: MainModuleConfigurator.build())
//    }
    
    fileprivate func openRootViewController() {
        window?.rootViewController = UINavigationController(rootViewController: ViewController())
    }
}
