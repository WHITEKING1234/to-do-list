//
//  TODOModuleBuilder.swift
//  To Do List
//
//  Created by Mac on 23/5/23.
//  
//

import UIKit

final class TODOModuleConfigurator {
    /// Собрать модуль
    class func build() -> UIViewController {
        let view = TODOViewController()
        let presenter = TODOPresenter()
        let interactor = TODOInteractor()
        let router = TODORouter()

        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        router.viewController = view

        return view
    }
}
