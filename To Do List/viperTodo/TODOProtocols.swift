//
//  TODOProtocols.swift
//  To Do List
//
//  Created by Mac on 23/5/23.
//  
//

import Foundation

protocol TODOViewProtocol: AnyObject {
}

protocol TODOViewToPresenterProtocol: AnyObject {
}

protocol TODOInteractorProtocol: AnyObject {
}

protocol TODOInteractorToPresenterProtocol: AnyObject {
}

protocol TODORouterProtocol: AnyObject {
}
