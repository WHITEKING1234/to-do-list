//
//  TODOPresenter.swift
//  To Do List
//
//  Created by Mac on 23/5/23.
//  
//

import Foundation

final class TODOPresenter {
    
    weak var view: TODOViewProtocol?
    var interactor: TODOInteractorProtocol?
    var router: TODORouterProtocol?
    
}

extension TODOPresenter: TODOViewToPresenterProtocol {
}

extension TODOPresenter: TODOInteractorToPresenterProtocol {
}
